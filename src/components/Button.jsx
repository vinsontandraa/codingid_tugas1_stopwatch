import React from 'react'

const Button = (props) => {
    return (
        <div>
            {(props.status === 0) ?
                <div>
                    <button className='btn-reset' onClick={props.reset}>Reset</button>
                    <button className='btn-start' onClick={props.start}>Start</button>
                    <button className='btn-stop' onClick={props.stop}>Stop</button>
                </div> : ""
            }

            {(props.status === 1) ?
                <div>
                    <button className='btn-reset' onClick={props.reset}>Reset</button>
                    <button className='btn-start' onClick={props.start}>Start</button>
                    <button className='btn-stop' onClick={props.stop}>Stop</button>
                </div> : ""
            }

            {(props.status === 2) ?
                <div>
                    <button className='btn-reset' onClick={props.reset}>Reset</button>
                    <button className='btn-start' onClick={props.start}>Start</button>
                    <button className='btn-stop' onClick={props.stop}>Stop</button>
                </div> : ""
            }
        </div>

    )
}

export default Button;