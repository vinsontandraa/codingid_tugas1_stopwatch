import React from 'react'

const Timer = (props) => {

    return (
        <div className='timer-container'>
            <div className='timer-text'>
                <span>{props.time.h}</span>&nbsp;:&nbsp;
                <span>{props.time.m}</span>&nbsp;:&nbsp;
                <span>{props.time.s}</span>
            </div>
            <div>
                <span>Jam</span>&nbsp;:&nbsp;
                <span>Menit</span>&nbsp;:&nbsp;
                <span>Detik</span>
            </div>
        </div>

    )
}

export default Timer;